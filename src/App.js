import React from 'react';

import Container from '@material-ui/core/Container';
import { HashRouter as Router, Route, Switch} from 'react-router-dom';

import Login from './components/Login'
import Main from './components/Main'

function App() {
  return (
    <Router>
      
      <Container>
        <Switch>
          <Route exact path="/">
            <Main/>
          </Route>
          <Route path="/login">
            <Login/>
          </Route>
        </Switch>
      </Container>
    </Router>
    
  );
}

export default App;
