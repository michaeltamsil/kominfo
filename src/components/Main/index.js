import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import WallpaperIcon from '@material-ui/icons/Wallpaper';

const Index = props => {
    return (
        <div>
            <Box style={{ display: 'flex'}}>
                    <Box flexGrow={1}>KOMINFO</Box>
                    <Button color="primary" style={{textTransform: 'none'}} variant="contained">Masuk</Button>
                    <Button style={{textTransform: 'none'}} variant="contained">Daftar</Button>
            </Box>
            <Typography align="center" variant="h5">Aplikasi Layanan Publik</Typography>
            <Typography align="center">Direktorat Jendral Aplikasi Informatika</Typography>
            
            <Grid container >
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>PSE Lingkup Privat</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>SICANTIK Cloud</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>Mail GO.ID</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>Domain GO.ID</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>PSE Penyelenggara Negara</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>IGRS</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>Penyelenggara Sertifikasi Elektronik</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>Aduan Konten</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>Help desk</Typography>
                    </Paper>
                </Grid>
                <Grid item sm={3} xs={4}>
                    <Paper align="center">
                        <WallpaperIcon/>
                        <Typography>Layanan Kominfo.go.id</Typography>
                    </Paper>
                </Grid>
            </Grid>
            <Typography align="center">KEMENTRIAN KOMUNIKASI & INFORMATIKA 2021</Typography>
            <Typography align="center">HUBUNGI KAMI : 159</Typography>
        </div>
    )
}

export default Index;